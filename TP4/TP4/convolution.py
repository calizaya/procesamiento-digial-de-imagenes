import tkinter as tk
import os
import numpy as np
import matplotlib

from tkinter import ttk, filedialog
from PIL import Image, ImageTk

from scipy import signal

class Kernel:
    def get(self, n):
        pass

class FlatKernel(Kernel):
    def get(self, n=1):
        kernel = np.ones((n*2+1,n*2+1))
        return kernel / np.sum(kernel)

class BarlettKernel(Kernel):
    def get(self, n):
        vec = np.concatenate([np.linspace(1.0, n+1, n+1), np.linspace(n,1,n)])
        [x,y] = np.meshgrid(vec, vec)
        kernel = x*y
        return kernel / np.sum(kernel)

class GaussKernel(Kernel):
    def get(self, n=3):
        g = signal.gaussian(n,1)
        kernel = np.outer(g, g.T)
        return kernel / kernel.sum()

class LaplaceKernel(Kernel):
    def __init__(self, n=4):
        self.n = n

    def get(self, n=4):
        if self.n == 4:
            kernel = np.array([[0.,-1.,0.],[-1,4.,-1.],[0.,-1.,0.]])
        if self.n == 8:
            kernel = np.array([[-1.,-1.,-1.],[-1,8.,-1.],[-1.,-1.,-1.]])
        kernel /= np.sum(np.abs(kernel))
        return kernel

def check_and_resize(image):
    w,h = image.size
    if w > 390:
        h = int(h * (390/w))
        w = 390
    if h > 400:
        w = int(w * (400/h))
        h = 400
    return image.resize((w, h))

def open_image(dest, size_lbl):
    filename = filedialog.askopenfilename(initialdir=os.getcwd(), title="Choose an image file", filetypes = (("jpg files","*.jpg"),("png files","*.png"),("bmp files","*.bmp")))
    if filename == () or filename is None or filename == '':
        return
    image = Image.open(filename, 'r')

    w,h = image.size
    size_lbl.set(str(w) + 'x' + str(h))
    image_resized = check_and_resize(image)
    image_for_lbl = ImageTk.PhotoImage(image_resized)
    dest.configure(image=image_for_lbl)
    dest.image = image_for_lbl
    return image

def open_and_load_image():
    global img
    global img_yiq
    img = open_image(image_a_lbl, img_a_size_lbl)
    check_conditions()

def check_conditions(event=None):
    if img is not None and filter_selected.get() != '':
        process_btn['state'] = tk.ACTIVE
    else:
        process_btn['state'] = tk.DISABLED

def process_image():
    kernel_function = kernel_map[filter_selected.get()]
    kernel = kernel_function.get(3)
    result = signal.convolve(img, kernel, 'valid')
    new_img = Image.fromarray(np.uint8(result))
    gray_image = ImageTk.PhotoImage(new_img)
    image_b_lbl.configure(image=gray_image)
    image_b_lbl.image = gray_image

filters = ['', 'Flat', 'Barlett', 'Gaussian', 'Laplace 4', 'Laplace 8']
img = None

kernel_map = {
    'Flat': FlatKernel(),
    'Barlett': BarlettKernel(),
    'Gaussian': GaussKernel(),
    'Laplace 4': LaplaceKernel(4),
    'Laplace 8': LaplaceKernel(8)
}

window = tk.Tk()
window.title('IDPI Convolution')
window.geometry('800x520')

image_a_lbl = tk.Label(window)
image_a_lbl.place(x=10, y=10)

image_b_lbl = tk.Label(window)
image_b_lbl.place(x=400, y=10)

img_a_size_lbl = tk.StringVar()
tk.Label(window, textvariable=img_a_size_lbl).place(x=180, y=400)
img_b_size_lbl = tk.StringVar()
tk.Label(window, textvariable=img_b_size_lbl).place(x=580, y=400)

tk.Button(text='Open Image', command=open_and_load_image).place(x=150, y=420)
process_btn = tk.Button(text='Process', state=tk.DISABLED, command=process_image)
process_btn.place(x=470, y=420)

filter_selected = tk.StringVar()
filter_cmb = ttk.Combobox(values=filters, textvariable=filter_selected, state='readonly')
filter_cmb.bind('<<ComboboxSelected>>', check_conditions)
filter_cmb.place(x=130, y=470)

tk.Label(window, text='Filter').place(x=170, y=490)

message = tk.StringVar()
tk.Label(window, textvariable=message).place(x=510, y=470)

window.mainloop()