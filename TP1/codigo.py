import tkinter as tk
import os
import numpy as np

from tkinter import ttk,filedialog
from PIL import Image, ImageTk

def showimage():
    fln = filedialog.askopenfilename(initialdir=os.getcwd(), title="Elija un archivo de imagen")
    img = Image.open(fln, 'r')
    width,height = img.size
    if (width > 480):
        height = int(height * (480/width))
        width = 480
    if (height > 540):
        width = int(width * (540/height))
        height = 540
    global img_resize
    img_resize = img.resize((width,height))
    imgtk = ImageTk.PhotoImage(img_resize)
    lbl_original.configure(image=imgtk)
    lbl_original.image = imgtk

def procesar_imagen():
    is_float = lambda x: x.replace('.','',1).isdigit() and "." in x
    if (inp_a_param.get() is None or inp_a_param.get() == ""):
        text_msg.set("El parametro 'a' esta en blanco")
        return
    if (inp_b_param.get() is None or inp_b_param.get() == ""):
        text_msg.set("El parametro 'b' esta en blanco")
        return
    if not is_float(inp_b_param.get()) or not is_float(inp_a_param.get()):
        text_msg.set("Los parametros no son valores punto flotante")
        return
    a = float(inp_a_param.get())
    b = float(inp_b_param.get())

    h,w = img_resize.size
    text_msg.set('Procesando')
    pixels= np.asarray(img_resize, dtype='float32') / 255.0
    m_yiq = np.zeros((w,h,3))
    m_yiq[:,:,0] = a * (0.299 * pixels[:,:,0] + 0.587 * pixels[:,:,1] + 0.114 * pixels[:,:,2])
    m_yiq[:,:,1] = b * (0.595716 * pixels[:,:,0] + -0.274453 * pixels[:,:,1] + -0.321263 * pixels[:,:,2])
    m_yiq[:,:,2] = b * (0.211456 * pixels[:,:,0] + -0.522591 * pixels[:,:,1] + 0.311135 * pixels[:,:,2])

    m_yiq[:,:,0] = np.where(m_yiq[:,:,0] > 1, 1, m_yiq[:,:,0])
    m_yiq[:,:,1] = np.where(m_yiq[:,:,1] < -0.5957, -0.5957, m_yiq[:,:,1])
    m_yiq[:,:,1] = np.where(m_yiq[:,:,1] > 0.5957, 0.5957, m_yiq[:,:,1])
    m_yiq[:,:,2] = np.where(m_yiq[:,:,2] < -0.5226, -0.5226, m_yiq[:,:,2])
    m_yiq[:,:,2] = np.where(m_yiq[:,:,2] > 0.5226, 0.5226, m_yiq[:,:,2])

    pixels[:,:,0] = 1 * m_yiq[:,:,0] + 0.9663 * m_yiq[:,:,1] + 0.6210 * m_yiq[:,:,2]
    pixels[:,:,1] = 1 * m_yiq[:,:,0] + -0.2721 * m_yiq[:,:,1] + -0.6474 * m_yiq[:,:,2]
    pixels[:,:,2] = 1 * m_yiq[:,:,0] + -1.1070 * m_yiq[:,:,1] + 0.311135 * m_yiq[:,:,2]

    pixels *= 255
    pil_image= Image.fromarray(np.uint8(pixels)).convert('RGB')
    imgtk = ImageTk.PhotoImage(pil_image)
    lbl_procesado.configure(image=imgtk)
    lbl_procesado.image = imgtk
    text_msg.set('Imagen Procesada')
    
        

root = tk.Tk()
root.title("IPDI Trabajo Práctico Nº 1")
root.geometry("1000x600")

lbl_original = tk.Label(root)
lbl_original.place(x=10,y=40)

lbl_procesado = tk.Label(root)
lbl_procesado.place(x=510,y=40)

boton_abrir_archivo = ttk.Button(text="Abrir archivo", command=showimage)
boton_abrir_archivo.place(x=10, y=10)

lbl_a_param = tk.Label(root, text="a=")
lbl_a_param.place(x=120,y=10)

inp_a_param = tk.Entry(root)
inp_a_param.insert(0,"1.0")
inp_a_param.place(x=140,y=10)

lbl_b_param = tk.Label(root, text="b=")
lbl_b_param.place(x=280,y=10)

inp_b_param = tk.Entry(root)
inp_b_param.insert(0,"1.0")
inp_b_param.place(x=300,y=10)

boton_procesar = ttk.Button(text="Procesar", command=procesar_imagen)
boton_procesar.place(x=510, y=10)

text_msg = tk.StringVar()

lbl_msg = tk.Label(root, textvariable=text_msg)
lbl_msg.place(x=600, y=10)

img_resize = None

root.mainloop()
