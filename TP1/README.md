# Trabajo Práctico Nº 1

## Desarrollo
Para el desarrollo del presente trabajo se utilizaron las librerías de Numpy (para la manipulación numérica), tkinter (para la generación de las pantallas de usuario) y PIL (para la visualización de las imágenes)

Para ello se siguio el proceso dado en clases, donde los pasos mas importantes, es la conversión de RGB a YIQ y viceversa, para ello se emplearon las matrices dadas en clase, y junto con la librería numpy se aplicaron los cálculos correspondientes.

Para la verificación de los valores obtenidos luego de aplicar los parámetros __"a"__ y __"b"__ se decidió aplicar la condición de que a todos los valores que esten por fuera de rango, se le aplicá el valor máximo o mínimo según corresponda.

## Ejecución

Para poder ejecutar el código solo debe ejecutarse desde consola con el siguiente comando:

```bash
python codigo.py
```

Esto iniciará el programa desplegando la siguente pantalla:

![Screenshot](./recursos/principal.png)

En esta pantalla lo primero a ralizar es la elección de la imagen a procesar, esto se hace por medio del boton ___"Abrir Archivo"___, esto levantará un modal donde se puede elegir la imagen que se desea procesar.

Para nuestro caso, se agregó la imagen que se utilizó para las pruebas, que se verán mas adelante.

![Screenshot](./recursos/apertura_imagen.png)

Este es el archivo llamado "imagen.png" que se encuentra junto al código del programa.

## Pruebas

Las pruebas realizadas para la verificación del código fueron las siguientes:

Disminución de la luminosidad (a = 0.6)
![Screenshot](./recursos/luminosidad_baja.png)

Aumento de la luminosidad (a = 1.5)
![Screenshot](./recursos/luminosidad_alta.png)

Disminución de la saturación (a = 0.5)
![Screenshot](./recursos/saturacion_baja.png)

Aumento de la saturación (a = 1.6)
![Screenshot](./recursos/saturacion_alta.png)

Imagen en blanco y negro, es decir saturacion en 0 (b = 0), con luminosidad aumentada (a = 1.3)
![Screenshot](./recursos/blanco_negro_luminosidad_alta.png)

Combinación de baja luminosidad (a = 0.6) y baja saturación (b = 0.6)
![Screenshot](./recursos/combinado.png)
