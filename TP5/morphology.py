import imageio
import matplotlib
import os
import matplotlib.pyplot as plt
import numpy as np
import tkinter as tk

from os.path import exists
from tkinter import ttk, filedialog
from PIL import Image, ImageTk
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg,
    NavigationToolbar2Tk
)
from matplotlib.ticker import PercentFormatter

matplotlib.use('TkAgg')

MAT_YIQ = np.array([[0.299, 0.595716, 0.211456],[0.587, -0.274453, -0.522591],[0.114, -0.321263, 0.311135]])

def box(r):
  se = np.ones((r*2+1,r*2+1),dtype=np.bool)
  return se

def circle(r, threshold = 0.3):
  vec = np.linspace(-r,r,r*2+1)
  [x,y] = np.meshgrid(vec,vec)
  se = (x**2 + y**2)**0.5 < (r + threshold)
  return se

def rgb2yiq(_im):
  return (_im.reshape((-1,3)) @ MAT_YIQ).reshape(_im.shape)

def yiq2rgb(_yiq):
  return (_yiq.reshape((-1,3)) @ np.linalg.inv(MAT_YIQ)).reshape(_yiq.shape)

def _morph_multiband(im,se,op):
  result = np.zeros(im.shape)
  offset = (np.array(se.shape)-1)//2
  im = np.pad(im, [(offset[0],offset[0]),(offset[1],offset[1]),(0,0)],'edge')
  for y,x in np.ndindex(result.shape[:2]):
    pixels = im[y:y+se.shape[0], x:x+se.shape[1]][se]
    result[y,x] = pixels[op(pixels[:,0])]
  return result

def _morph_color(im,se,op):
  im2 = (rgb2yiq(im)[:,:,0])[:,:,np.newaxis]
  im2 = np.concatenate((im2,im), axis=2)
  result = _morph_multiband(im2,se,op)[:,:,1]
  return result

def _morph_gray(im,se,op):
  result = np.zeros(im.shape)
  offset = (np.array(se.shape)-1)//2
  im = np.pad(im,[(offset[0],offset[0]),(offset[1],offset[1])],'edge')
  for y,x in np.ndindex(result.shape):
    pixels = im[y:y+se.shape[0],x:x+se.shape[1]][se]
    result[y,x] = op(pixels)
  return result

def im_dilate(im,se):
  if im.ndim == 3:
    return _morph_color(im,se,np.argmin)
  else:
    return _morph_gray(im,se,np.min)
    
def im_erode(im,se):
	if im.ndim == 3:
		return _morph_color(im,se,np.argmax)
	else:
		return _morph_gray(im,se,np.max)

def im_median(im,se):
	if im.ndim == 3:
		return _morph_color(im,se,lambda data: np.argsort(data)[len(data)//2])
	else:
		return _morph_gray(im,se,np.median)

def im_border_ext(im,se):
  return im_dilate(im,se)

def im_border_int(im,se):
  return im - im_erode(im,se)

def im_open(im,se):
  return im_dilate(im_erode(im,se),se)

def im_close(im,se):
  return im_erode(im_dilate(im,se),se)

def open_and_load_image():
  global img_a
  filename = filedialog.askopenfilename(initialdir=os.getcwd(), title="Choose an image file")
  if filename == () or filename is None or filename == '':
    return
  img_a = imageio.imread(filename)/255
  plt_a.imshow(img_a,'gray')
  canvas_a.draw()
  check_btn_filter()

def check_btn_filter(event=None):
  if img_a is not None and filter_selected.get() != '':
    process_btn['state'] = tk.ACTIVE
  else:
    process_btn['state'] = tk.DISABLED

def check_btn_copy():
  if img_b is not None:
    copy_btn['state'] = tk.ACTIVE
  else:
    copy_btn['state'] = tk.DISABLED

def process_image(): 
  global img_b
  operation = functions[filter_selected.get()]
  img_b = operation(img_a, se)
  plt_b.imshow(img_b,'gray')
  canvas_b.draw()
  check_btn_copy()

def copy_image():
  global img_a
  img_a = img_b
  plt_a.imshow(img_a,'gray')
  canvas_a.draw()

functions = {
  'Erosión': im_erode,
  'Dilatación': im_dilate,
  'Apertura': im_open,
  'Cierre': im_close,
  'Borde Interno': im_border_int,
  'Borde Externo': im_border_ext,
  'Mediana': im_median
}

img_a = None
img_b = None
se = circle(3)

window = tk.Tk()
window.title('IDPI Morphology')
window.geometry('830x430')

fig_a = Figure(figsize=(3.5,3.5), dpi=100)
plt_a = fig_a.add_subplot(111)
plt_a.axis('off')
canvas_a = FigureCanvasTkAgg(fig_a, window)
toolbar_a = NavigationToolbar2Tk(canvas_a, window, pack_toolbar=False)
toolbar_a.update()
canvas_a.draw()
canvas_a.get_tk_widget().place(x=10,y=10)

fig_b = Figure(figsize=(3.5,3.5), dpi=100)
plt_b = fig_b.add_subplot(111)
plt_b.axis('off')
canvas_b = FigureCanvasTkAgg(fig_b, window)
toolbar_b = NavigationToolbar2Tk(canvas_a, window, pack_toolbar=False)
toolbar_b.update()
canvas_b.draw()
canvas_b.get_tk_widget().place(x=470,y=10)

tk.Label(window, text='Imagen Original').place(x=140, y=370)
tk.Label(window, text='Imagen Filtrada').place(x=590, y=370)

tk.Button(text='Cargar', command=open_and_load_image).place(x=150, y=390)
process_btn = tk.Button(text='Filtrar ->', state=tk.DISABLED, command=process_image)
process_btn.place(x=370, y=100)
copy_btn = tk.Button(text='<- Copiar', state=tk.DISABLED, command=copy_image)
copy_btn.place(x=373, y=140)

filter_selected = tk.StringVar()
filter_cmb = ttk.Combobox(values=list(functions.keys()), textvariable=filter_selected, state='readonly')
filter_cmb.bind('<<ComboboxSelected>>', check_btn_filter)
filter_cmb.place(x=350, y=390)

window.mainloop()