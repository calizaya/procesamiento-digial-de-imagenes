from cgitb import text
import tkinter as tk
import os
import numpy as np

from os.path import exists
from tkinter import ttk, filedialog
from PIL import Image, ImageTk

class Operation:
    def process(self, image_a, image_b):
        pass

    # Convert YIQ to RGB
    def yiq_to_rgb(yiq_image):
        w = yiq_image.shape(0)
        h = yiq_image.shape(1)
        m_rgb = np.zeros((w,h,3))
        m_rgb[:,:,0] = 1 * yiq_image[:,:,0] + 0.9663 * yiq_image[:,:,1] + 0.6210 * yiq_image[:,:,2]
        m_rgb[:,:,1] = 1 * yiq_image[:,:,0] + -0.2721 * yiq_image[:,:,1] + -0.6474 * yiq_image[:,:,2]
        m_rgb[:,:,2] = 1 * yiq_image[:,:,0] + -1.1070 * yiq_image[:,:,1] + 0.311135 * yiq_image[:,:,2]
        return m_rgb


class RGBClampedSum(Operation):
    def process(self, image_a, image_b):
        sum = image_a + image_b
        return np.where(sum > 1, 1, sum)

class RGBAveragedSum(Operation):
    def process(self, image_a, image_b):
        return (image_a + image_b) / 2

class RGBClampedSubstract(Operation):
    def process(self, image_a, image_b):
        substract = image_a - image_b
        return np.where(substract >= 0, substract , 0)

class RGBAveragedSubstract(Operation):
    def process(self, image_a, image_b):
        substract = 0.5 + (image_a - image_b) / 2
        return np.where(substract < 0, 0 , substract)

class RGBMultiply(Operation):
    def process(self, image_a, image_b):
        return image_a * image_b

class RGBDivide(Operation):
    def process(self, image_a, image_b):
        divide = image_a / np.where(image_b == 0, 1, image_b)
        return np.where(divide > 1, 1, divide)

class RGBAbsoluteSubstract(Operation):
    def process(self, image_a, image_b):
        substract = abs(image_a - image_b)
        return substract

class YIQClampedSum(Operation):
    def process(self, image_a, image_b):
        w = len(image_a)
        h = len(image_a[0])
        sum = np.zeros((w,h,3))
        sum[:,:,0] = image_a[:,:,0] + image_b[:,:,0]
        sum[:,:,0] = np.where(sum[:,:,0] > 1, 1, sum[:,:,0])
        sum[:,:,1] = (image_a[:,:,0]*image_a[:,:,1] + image_b[:,:,0]*image_b[:,:,1]) / (image_a[:,:,0] + image_b[:,:,0])
        sum[:,:,1] = np.where(abs(sum[:,:,1]) > 0.5957, np.sign(sum[:,:,1]) * 0.5957, sum[:,:,1])
        sum[:,:,2] = (image_a[:,:,0]*image_a[:,:,2] + image_b[:,:,0]*image_b[:,:,2]) / (image_a[:,:,0] + image_b[:,:,0])
        sum[:,:,2] = np.where(abs(sum[:,:,2]) > 0.5226, np.sign(sum[:,:,2]) * 0.5226, sum[:,:,2])
        return sum

class YIQAveragedSum(Operation):
    def process(self, image_a, image_b):
        w = len(image_a)
        h = len(image_a[0])
        sum = np.zeros((w,h,3))
        sum[:,:,0] = (image_a[:,:,0] + image_b[:,:,0]) / 2
        sum[:,:,1] = (image_a[:,:,0]*image_a[:,:,1] + image_b[:,:,0]*image_b[:,:,1]) / (image_a[:,:,0] + image_b[:,:,0])
        sum[:,:,1] = np.where(abs(sum[:,:,1]) > 0.5957, np.sign(sum[:,:,1]) * 0.5957, sum[:,:,1])
        sum[:,:,2] = (image_a[:,:,0]*image_a[:,:,2] + image_b[:,:,0]*image_b[:,:,2]) / (image_a[:,:,0] + image_b[:,:,0])
        sum[:,:,2] = np.where(abs(sum[:,:,2]) > 0.5226, np.sign(sum[:,:,2]) * 0.5226, sum[:,:,2])
        return sum

class YIQIfLigther(Operation):
    def process(self, image_a, image_b):
        w = len(image_a)
        h = len(image_a[0])
        img = np.zeros((w,h,3))
        img[:,:,0] = np.where(image_a[:,:,0] > image_b[:,:,0], image_a[:,:,0], image_b[:,:,0])
        img[:,:,1] = np.where(image_a[:,:,0] > image_b[:,:,0], image_a[:,:,1], image_b[:,:,1])
        img[:,:,2] = np.where(image_a[:,:,0] > image_b[:,:,0], image_a[:,:,2], image_b[:,:,2])
        return img

class YIQIfDarker(Operation):
    def process(self, image_a, image_b):
        w = len(image_a)
        h = len(image_a[0])
        img = np.zeros((w,h,3))
        img[:,:,0] = np.where(image_a[:,:,0] < image_b[:,:,0], image_a[:,:,0], image_b[:,:,0])
        img[:,:,1] = np.where(image_a[:,:,0] < image_b[:,:,0], image_a[:,:,1], image_b[:,:,1])
        img[:,:,2] = np.where(image_a[:,:,0] < image_b[:,:,0], image_a[:,:,2], image_b[:,:,2])
        return img

def clamped_sum(image_a, image_b):
    sum = image_a + image_b
    return np.where(sum > 1, 1, sum)

def averaged_sum(image_a, image_b):
    return (image_a + image_b) / 2

# Convert RGB to YIQ
def rgb_to_yiq(rgb_image, original):
    h,w = original.size
    m_yiq = np.zeros((w,h,3))
    m_yiq[:,:,0] = 0.299 * rgb_image[:,:,0] + 0.587 * rgb_image[:,:,1] + 0.114 * rgb_image[:,:,2]
    m_yiq[:,:,1] = 0.595716 * rgb_image[:,:,0] + -0.274453 * rgb_image[:,:,1] + -0.321263 * rgb_image[:,:,2]
    m_yiq[:,:,2] = 0.211456 * rgb_image[:,:,0] + -0.522591 * rgb_image[:,:,1] + 0.311135 * rgb_image[:,:,2]
    return m_yiq

# Convert YIQ to RGB
def yiq_to_rgb(yiq_image, original):
    h,w = original.size
    m_rgb = np.zeros((w,h,3))
    m_rgb[:,:,0] = 1 * yiq_image[:,:,0] + 0.9663 * yiq_image[:,:,1] + 0.6210 * yiq_image[:,:,2]
    m_rgb[:,:,1] = 1 * yiq_image[:,:,0] + -0.2721 * yiq_image[:,:,1] + -0.6474 * yiq_image[:,:,2]
    m_rgb[:,:,2] = 1 * yiq_image[:,:,0] + -1.1070 * yiq_image[:,:,1] + 0.311135 * yiq_image[:,:,2]
    return m_rgb

def check_and_resize(image):
    w,h = image.size
    if w > 390:
        h = int(h * (390/w))
        w = 390
    if h > 400:
        w = int(w * (400/h))
        h = 400
    return image.resize((w, h))

def open_image(dest, size_lbl):
    filename = filedialog.askopenfilename(initialdir=os.getcwd(), title="Choose an image file", filetypes = (("jpg files","*.jpg"),("png files","*.png")))
    if filename == () or filename is None or filename == '':
        return
    image = Image.open(filename, 'r')

    w,h = image.size
    size_lbl.set(str(w) + 'x' + str(h))
    image_resized = check_and_resize(image)
    image_for_lbl = ImageTk.PhotoImage(image_resized)
    dest.configure(image=image_for_lbl)
    dest.image = image_for_lbl
    return image

def check_image_sizes():
    if img_a is not None and img_b is not None:
        if img_a.size != img_b.size:
            message.set("The images haven't equals size")
            process_btn['state'] = tk.DISABLED
        else:
            message.set("")
            check_process_button()
    else:
        message.set('')

def open_image_a():
    global img_a
    global img_rgb_a
    global img_yiq_a
    img_a = open_image(image_a_lbl, img_a_size_lbl)
    check_image_sizes()
    if img_a is not None:
        img_rgb_a = np.asarray(img_a, dtype='float') / 255.0
        img_yiq_a = rgb_to_yiq(img_rgb_a, img_a)

def open_image_b():
    global img_b
    global img_rgb_b
    global img_yiq_b
    img_b = open_image(image_b_lbl, img_b_size_lbl)
    check_image_sizes()
    if img_b is not None:
        img_rgb_b = np.asarray(img_b, dtype='float') / 255.0
        img_yiq_b = rgb_to_yiq(img_rgb_b, img_b)

def process_image():
    image_a = img_rgb_a if format_selected.get() == 'RGB' else img_yiq_a
    image_b = img_rgb_b if format_selected.get() == 'RGB' else img_yiq_b
    operation = functions[(operation_selected.get(), format_selected.get())]
    
    new_img = operation.process(image_a, image_b)

    if format_selected.get() == 'YIQ':
        new_img = yiq_to_rgb(new_img, img_a)

    new_img *= 255
    
    new_img = Image.fromarray(np.uint8(new_img)).convert('RGB')

    new_img = check_and_resize(new_img)
    rgb_image = ImageTk.PhotoImage(new_img)
    image_c_lbl.configure(image=rgb_image)
    image_c_lbl.image = rgb_image

def check_process_button(event=None):
    if operation_selected.get() != '' and format_selected.get() != '' and img_a is not None and img_b is not None:
        process_btn['state'] = tk.ACTIVE
    else:
        process_btn['state'] = tk.DISABLED

def change_format(event=None):
    if format_selected.get() == 'YIQ':
        operation_cmb['values'] = ['', 'Suma Clampleada', 'Suma Promediada', 'Resta Clampeada', 'Resta Promediada', 'if-ligther', 'if_darker']
    else:
        operation_cmb['values'] = ['', 'Suma Clampleada', 'Suma Promediada', 'Resta Clampeada', 'Resta Promediada', 'Resta Valor Absoluto', 'Producto', 'Cociente']
    operation_selected.set('')
    check_process_button()


formats = ['', 'RGB', 'YIQ']
img_a = None
img_rgb_a = None
img_yiq_a = None
img_b = None
img_rgb_b = None
img_yiq_b = None

functions = {
    ('Suma Clampleada', 'RGB'): RGBClampedSum(),
    ('Suma Promediada', 'RGB'): RGBAveragedSum(),
    ('Resta Clampeada', 'RGB'): RGBClampedSubstract(),
    ('Resta Promediada', 'RGB'): RGBAveragedSubstract(),
    ('Resta Valor Absoluto', 'RGB'): RGBAbsoluteSubstract(),
    ('Producto', 'RGB'): RGBMultiply(),
    ('Cociente', 'RGB'): RGBDivide(),
    ('Suma Clampleada', 'YIQ'): YIQClampedSum(),
    ('Suma Promediada', 'YIQ'): YIQAveragedSum(),
    ('if-ligther', 'YIQ'): YIQIfLigther(),
    ('if_darker', 'YIQ'): YIQIfDarker()
}

window = tk.Tk()
window.title('IDPI Pixel Arithmetic')
window.geometry('1200x520')

image_a_lbl = tk.Label(window)
image_a_lbl.place(x=10, y=10)
image_b_lbl = tk.Label(window)
image_b_lbl.place(x=405, y=10)
image_c_lbl = tk.Label(window)
image_c_lbl.place(x=800, y=10)

img_a_size_lbl = tk.StringVar()
tk.Label(window, textvariable=img_a_size_lbl).place(x=180, y=400)
img_b_size_lbl = tk.StringVar()
tk.Label(window, textvariable=img_b_size_lbl).place(x=580, y=400)

tk.Button(text='Open Image 1', command=open_image_a).place(x=150, y=420)
tk.Button(text='Open Image 2', command=open_image_b).place(x=550, y=420)
process_btn = tk.Button(text='Process', state=tk.DISABLED, command=process_image)
process_btn.place(x=870, y=420)
tk.Button(text='Guardar', state=tk.DISABLED).place(x=970, y=420)
tk.Button(text='Salir', command=exit).place(x=1070, y=420)

format_selected = tk.StringVar()
format_cmb = ttk.Combobox(values=formats, textvariable=format_selected, state='readonly')
format_cmb.bind('<<ComboboxSelected>>', change_format)
format_cmb.place(x=130, y=470)
operation_selected = tk.StringVar()
operation_cmb = ttk.Combobox(values=[], textvariable=operation_selected, state='readonly')
operation_cmb.bind('<<ComboboxSelected>>', check_process_button)
operation_cmb.place(x=530, y=470)

tk.Label(window, text='Format').place(x=170, y=490)
tk.Label(window, text='Operation').place(x=570, y=490)

message = tk.StringVar()
tk.Label(window, textvariable=message).place(x=810, y=470)

window.mainloop()